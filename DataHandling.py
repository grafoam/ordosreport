import pandas as pd
import datetime as dt
import requests
from pandas.tseries.offsets import BDay


def DataFilter(data, datafull, testyear, division, mindelta):

    full_data_filter = datafull[(datafull['year'].isin(testyear))
                                & (datafull['division'] == division)]
    full_data_filter = full_data_filter.groupby(['t0', 'book', 'year'])['MtR_MtM1'].sum().reset_index()
    full_data_filter = full_data_filter[(full_data_filter['t0']==min(full_data_filter['t0']))|
                                        (full_data_filter['t0'] == max(full_data_filter['t0']))]
    full_data_filter['MtR_MtM1'] = full_data_filter['MtR_MtM1'].apply(lambda x: x / 1000000)
    full_data_filter = full_data_filter.pivot_table(index=['book', 'year'], columns='t0',
                                                    values='MtR_MtM1').reset_index()
    full_data_filter['year'] = full_data_filter['year'].astype(int)
    number_date = len(full_data_filter.loc[:, full_data_filter.dtypes == float].columns)
    number_col = len(full_data_filter.columns)
    full_data_filter['Delta'] = full_data_filter.iloc[:, number_col - 1] - full_data_filter.iloc[:,
                                                                           number_col - number_date]
    full_data_filter = full_data_filter[
        (full_data_filter['Delta'] > float(mindelta)) | (full_data_filter['Delta'] < -float(mindelta))]
    full_data_filter.drop('Delta', 1)

    data_filter = data[(data['year'].isin(testyear)) & (data['division'] == division)]
    data_filter = data_filter.groupby(['t0', 'year']).sum().reset_index()

    return data_filter, full_data_filter

def DataFilterCountry(data, datafull, testyear, division, mindelta):

    data_filter = datafull[(datafull['year'].isin(testyear)) & (datafull['division'] == division)]
    data_filter = data_filter[['t0','year','strategy','MtR_MtM1']]
    data_filter['MtR_MtM1'] = data_filter['MtR_MtM1'].apply(lambda x: x / 1000000)
    data_filter = data_filter.groupby(['t0', 'year', 'strategy']).sum().reset_index()

    return data_filter

def PricesFilter(prices_table,curve,ts):
    prices_table.drop(['rftype','label','mat_end','mat_start','confidential','curve_type', 'riskfactor'], axis = 1, inplace=True)
    prices_table = prices_table[(prices_table['ts']==ts) & (prices_table['curve'].isin(curve))]
    return prices_table


def BookFilter(data, datafull, book, division):
    full_data_filter = datafull[(datafull['book']==book) & (datafull['division'] == division)]
    full_data_filter = full_data_filter.groupby(['t0', 'book', 'year'])['MtR_MtM1'].sum().reset_index()
    full_data_filter['MtR_MtM1'] = full_data_filter['MtR_MtM1'].apply(lambda x: x / 1000000)
    full_data_filter['year'] = full_data_filter['year'].astype(int)
    return full_data_filter

def extract_full_table():
    final_data = pd.DataFrame()
    start_date = dt.date(2017, 5, 15)
    end_date = dt.date.today()
    day_count = (end_date - start_date).days + 1

    for date1 in (start_date + dt.timedelta(n) for n in range(day_count)):
        if date1.isoweekday() not in (6,7):
            date1_str = date1.strftime('%Y-%m-%d')
            date0_str = (date1 - BDay(1)).strftime('%Y-%m-%d')
            date0_str2 = (date1 - BDay(1)).strftime('%Y%m%d')
            native_url = 'http://rms.gdfsuez.net:8310/ordos/explain_ebitda_mtm_cf_yearly'
            url = native_url+'?t0=' +date0_str+ '&t1=' +date1_str+'&label=PAM'
            print(url)
            session = requests.Session()
            session.trust_env = False
            response = session.get(url)
            my_data = pd.read_json(response.content)
            if not my_data.empty:
                my_data['MtR_MtM1'] = my_data['mtr1'] + my_data['mtm1']
                final_data = pd.concat([final_data,my_data])
    my_data_clean = final_data[['t0', 't1', 'year', 'division', 'strategy', 'MtR_MtM1']]
    outplot = my_data_clean.groupby(['t0','year','division']).sum().reset_index()
    outplot.t0.apply(lambda x: str(x))
    outplot['MtR_MtM1'] = outplot['MtR_MtM1'] / 1000000
    outplot['MtR_MtM1'] = outplot['MtR_MtM1'].astype('int')
    return outplot,final_data

def offlinedata():
    lightdata = pd.read_csv('dummycsv.csv')
    fulldata = pd.read_csv('fulldummycsv.csv')
    return lightdata,fulldata

def extract_price_table(curve):
    final_data = pd.DataFrame()
    start_date = dt.date(2017, 5, 15)
    end_date = dt.date.today()

    day_count = (end_date - start_date).days + 1

    date_from = start_date.strftime('%Y-%m-%d')
    date_to = end_date.strftime('%Y-%m-%d')
    native_url = 'http://rms.gdfsuez.net:8310/ordos/forward_price'
    url = native_url + '?refdate_from=' + date_from + '&refdate_to=' + date_to + '&curve=' + curve  + '&label=EC&rftype=PURE*&mat=Y*'
    session = requests.Session()
    session.trust_env = False
    response = session.get(url)
    my_data = pd.read_json(response.content)
    final_data = final_data.append(my_data)

    return final_data