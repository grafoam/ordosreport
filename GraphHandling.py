
import random
from bokeh.models import (HoverTool, FactorRange, LinearAxis, Grid,Range1d, Legend)
from bokeh.plotting import figure
import Dictionnaries


def create_hover_tool():
    """Generates the HTML for the Bokeh's hover data tool on our graph."""
    hover_html = """
      <div>
        <span class="hover-tooltip">$x</span>
      </div>
      <div>
        <span class="hover-tooltip">@y M€ </span>
      </div>
    """
    return HoverTool(tooltips=hover_html)

def create_chart(data, splitter, title, x_name, y_name, hover_tool=None,
                     width=1200, height=300):
    """Creates a bar chart plot with the exact styling for the centcom
       dashboard. Pass in data as a dictionary, desired plot title,
       name of x axis, y axis and the hover tool HTML.
    """

    xdr = FactorRange(factors=data[x_name].drop_duplicates().astype(str).values.tolist())

    if min(data[y_name]) > 0:
        ydr = Range1d(start=min(data[y_name]) * 0.9, end=max(data[y_name]) * 1.1)
    else:
        ydr = Range1d(start=min(data[y_name]) * 1.1, end=max(data[y_name]) * 1.1)


    tools = []
    if hover_tool:
        tools = [hover_tool, ]

    plot = figure(title=title, x_range=xdr, y_range=ydr,
                  h_symmetry=False, v_symmetry=False,
                  min_border=0, toolbar_location="above", tools=tools,
                  responsive=True, outline_line_color="#666666", height=height)
    items=[]
    for year in data[splitter].drop_duplicates().astype('int'):
        data_filtered = data[data['year'] == year]
        r = lambda: random.randint(0, 255)
        p = plot.line(x=data_filtered[x_name], y=data_filtered[y_name], legend=None,
                  line_width=2, line_color="#%02X%02X%02X" % (r(), r(), r()))
        items.append((str(year),[p]))
        # source = ColumnDataSource(data[data['year'] == year])
        #glyph = VBar(x=x_name, top=y_name, bottom=0, width=.8,fill_color="#%02X%02X%02X" % (r(),r(),r()))



    xaxis = LinearAxis()
    yaxis = LinearAxis()

    plot.add_layout(Legend(items=items,location = (5,int(height/2))),'right')
    plot.add_layout(Grid(dimension=0, ticker=xaxis.ticker))
    plot.add_layout(Grid(dimension=1, ticker=yaxis.ticker))
    plot.toolbar.logo = None
    plot.min_border_top = 0
    plot.xgrid.grid_line_color = None
    plot.ygrid.grid_line_color = "#999999"
    plot.yaxis.axis_label = "MtM and MtR"
    plot.ygrid.grid_line_alpha = 0.1
    plot.xaxis.axis_label = "Days of valuation"
    plot.xaxis.major_label_orientation = 1

    return plot

def create_chart_prices(data, splitter, curve, title, x_name, y_name, hover_tool=None,
                     width=1200, height=400):
    """Creates a bar chart plot with the exact styling for the centcom
       dashboard. Pass in data as a dictionary, desired plot title,
       name of x axis, y axis and the hover tool HTML.
    """

    data = data[data['curve']==curve]
    data.drop('curve',axis=1, inplace=True)

    xdr = FactorRange(factors=data[x_name].drop_duplicates().astype(str).values.tolist())
    if min(data[y_name])>0:
        ydr = Range1d(start=min(data[y_name]) * 0.9, end=max(data[y_name])*1.1)
    else:
        ydr = Range1d(start=min(data[y_name]) * 1.1 , end=max(data[y_name]) * 1.1)

    tools = []
    if hover_tool:
        tools = [hover_tool, ]

    plot = figure(title=title, x_range=xdr, y_range=ydr,
                  h_symmetry=False, v_symmetry=False,
                  min_border=0, toolbar_location="above", tools=tools,
                  responsive=True, outline_line_color="#666666", height=height)

    items = []
    for split in ['Y_00','Y_01','Y_02','Y_03']:
        data_filtered = data[data[splitter] == split]
        r = lambda: random.randint(0, 255)
        p = plot.line(x=data_filtered[x_name], y=data_filtered[y_name], legend=None,
                  line_width=2, line_color="#%02X%02X%02X" % (r(), r(), r()))
        items.append((str(Dictionnaries.Maturities[split]), [p]))

    xaxis = LinearAxis()
    yaxis = LinearAxis()

    plot.add_layout(Legend(items=items, location=(5, int(height / 2))), 'right')
    plot.add_layout(Grid(dimension=0, ticker=xaxis.ticker))
    plot.add_layout(Grid(dimension=1, ticker=yaxis.ticker))
    plot.toolbar.logo = None
    plot.min_border_top = 0
    plot.xgrid.grid_line_color = None
    plot.ygrid.grid_line_color = "#999999"
    plot.yaxis.axis_label = "Prices evolution"
    plot.ygrid.grid_line_alpha = 0.1
    plot.xaxis.axis_label = "Days of valuation"
    plot.xaxis.major_label_orientation = 1
    return plot