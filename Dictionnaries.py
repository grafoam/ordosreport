import datetime

AssetBooks = {
    "CCBG": "Drogenbos (OCGT)",
    "CCBX": "Waste Belgium & TJs",
    "EBPC": "GC, Inj. costs of BE CCGT/CHP",
    "ECBG": "Belgian CHP",
    "EOBG": "Belgian CCGT",
    "JVLE": "Lillo",
    "ZVPW": "Zandvliet",
    "ECNG": "Flevo 4/5",
    "IESO": "Eems all units",
    "GCNC": "Rotterdam",
    "XBAW": "Les Awirs",
    "XBKN": "Knippegroen",
    "XBMG": "Rodenhuize",
    "XBPS": "Coo",
    "XDCO": "Zolling 5",
    "XDOA": "Farge",
    "XDCW": "Willemshaven",
    "XFFG": "DK6 ENGIE",
    "XFDG": "DK6 Arcelor",
    "XFDR": "Appolinaire",
    "XFCM": "Montoir",
    "XFSG": "Cycofos / Combigolfe",
    "NUBC": "Chooz B",
    "TRIC": "Tricastin",
    "XFSH": "SHEM / Artouste",
    "XDCC": "Wuppertal",
    "XDNT": "Jansen/BKW/Helmstadt/Levanto",
    "XDPS": "Pfreim",
    "ECNW": "Eems Wind",
    "ECAP": "Air Product",
    "ELEV": "Delfzijl",
    "MGPB": "Max Green (old)",
    "CCBN": "Doel 1/2 & T1",
    "XBLN": "Doel 3/4 & T2/3",
    "IBNC": "Edmundo swap",
    "XBRE": "Wind Belgium",
    "COXL": "Portfolio proxy",
    "BEPC": "Portfolio Costs BNL BE",
    "MNGT": "Chooz B SPE-EBL swap",
    "XBTI": "Tihange 1 old book",
    "PCFB": "Portfolio Costs BNL FR"
}

now = datetime.datetime.now()

Maturities = {
    "Y_00": "CAL-" + str(now.year - 2000),
    "Y_01": "CAL-" + str(now.year + 1 - 2000),
    "Y_02": "CAL-" + str(now.year + 2 - 2000),
    "Y_03": "CAL-" + str(now.year + 3 - 2000),
    "Y_04": "CAL-" + str(now.year + 4 - 2000),
    "Y_05": "CAL-" + str(now.year + 5 - 2000)
}
