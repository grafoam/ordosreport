import pandas as pd
import numpy as np
from IPython.display import HTML
import re
import webbrowser

# Variable initialisation :
main_expo = pd.DataFrame()

# Display of DataFrame parameter :
pd.set_option('display.float_format', lambda x: '%.1f' % x)


# Directory management :
#file = "DAILYexposure_REF__MET_PAM.csv"
#path = r'//XS009323.WIN.CORP.COM/DFSRoot01/02_0063/22_REMTO/01_COMMON/TESTFILES/Charles/'
file = "dailyexposure_MET_PAM.csv"
path = r'//XS009323.WIN.CORP.COM/DFSRoot01/02_0063/22_REMTO/01_COMMON/2017/201706/10_REMTO/30_EXPO/'

# 4 Letters to filter the commodity.
ComFil = ['POWE','NATG','COAL']

# Open file :
full_data = pd.read_csv(path+file, delimiter=";")

# Filter the output :
mini_data = full_data[['DLVyear','division','RFTYPOLOGY','volumei']]


# Grouping / Summing :
main_expo = mini_data[mini_data['RFTYPOLOGY'].str[:4].isin(ComFil)].groupby(['division', 'DLVyear', 'RFTYPOLOGY']).sum()/1000000
main_expo = main_expo.reset_index()

# Pivoting the table to display year on column :
main_expo = main_expo.pivot_table(index=['division', 'RFTYPOLOGY'], columns='DLVyear', values='volumei', aggfunc='sum')

# Formatting :
main_expo = main_expo.replace(np.nan, '', regex=True)

# Create a string for output in HTML :
out_html = main_expo.to_html()
random_id = "id%d" % np.random.choice(np.arange(1000000))
out_html = re.sub(r'<table', r'<table id=%s ' % random_id, out_html)
out_html = re.sub(r'RFTYPOLOGY',r'Commodity', out_html)
out_html = re.sub(r'border=\"1\"', r'border="3"', out_html)
out_html = re.sub(r'POWER', r'Power', out_html)
out_html = re.sub(r'NATGAS', r'Gas', out_html)
out_html = re.sub(r'COAL', r'Coal', out_html)
out_html = re.sub(r'DLVyear', r'Year', out_html)
out_html = re.sub(r'division', r'BU', out_html)

style = """
<head>
<style>
    table#{random_id} tr:hover 
    {{
    background-color : #f5f5f5;
    }}
    td,th, table 
    {{
    text-align : center;
    border: 1px solid black;
    border-collapse: collapse;
    }}
    th 
    {{
    font-weight: 600;
    color: darkblue;
    padding: 5px;
    }}
</style>
</head>
""".format(random_id=random_id)

title = """
<h1>Latest exposure of PAM assets per division :</h1>
"""

out_html = style + title + out_html

# Write the html file :
f = open('temp.html', 'w')
f.write(out_html)
f.close()

# Open browser :
webbrowser.open('temp.html')