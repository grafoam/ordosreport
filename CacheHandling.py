import threading
import DataHandling
import time

def get_my_cache(cache_key, cache, cachetimeout):
    print(threading.current_thread().name)
    try:
        cacheval = cache[cache_key]
        return cacheval
    except KeyError:
        if cache_key == "MRMCache":
            cacheval1, cacheval2 = DataHandling.extract_full_table()
            #cacheval1, cacheval2 = DataHandling.offlinedata()
            cacheval = [cacheval1, cacheval2]
        if cache_key == "PriceCachePW*":
            cacheval = DataHandling.extract_price_table('PW*')

        cache.set(cache_key, cacheval, expire=cachetimeout)
        print(cache_key + " updated at : " + time.strftime("%b %d %Y - %H:%M:%S"))
        return cacheval[0], cacheval[1]

