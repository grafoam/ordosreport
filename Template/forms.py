from wtforms import Form, SelectField, SubmitField, BooleanField, FloatField, HiddenField
from datetime import datetime
import Dictionnaries

def create_form(year_start,year_end):
    class DynamicForm(Form): pass
    setattr(DynamicForm, 'active', SelectField(u'Please select your division : ', choices=[('BNL', 'Benelux'),
                                                                                           ('TG', 'Generation')]))
    for Year in range(year_start, year_end):
        setattr(DynamicForm, 'Check' + str(Year), BooleanField(str(Year)))
    setattr(DynamicForm, 'action', SubmitField(u'Get data'))
    setattr(DynamicForm, 'tablefilter', FloatField(u'Min MtM evolution to report (in M€) : ', default=1.0))
    form = DynamicForm()
    return form

def asset_form(booklist):
    class YearForm(Form) : pass
    setattr(YearForm,'form_name', HiddenField('AssetForm'))
    setattr(YearForm, 'bu', SelectField(u'Please select your division : ', choices=[('BNL', 'Benelux'),
                                                                                ('TG', 'Generation')],id="select_bu"))
    choices = []
    for book in booklist:
        choices.append((book, Dictionnaries.Maturities.get(book,book)))

    setattr(YearForm, 'book', SelectField(u'Please select your book : ', choices=choices, id="select_book"))
    setattr(YearForm, 'action', SubmitField(u'Get data'))
    form = YearForm()
    return form
