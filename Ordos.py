import requests
import pandas as pd
import datetime as dt
from swagger_parser import SwaggerParser
import matplotlib.pyplot as plt
from pandas.tseries.offsets import BDay

final_data= pd.DataFrame()
start_date = dt.date(2017, 5, 13)
end_date = dt.date.today()

day_count = (end_date - start_date).days + 1

date_from = start_date.strftime('%Y-%m-%d')
date_to = end_date.strftime('%Y-%m-%d')
curve = 'PW_BE'
native_url = 'http://rms.gdfsuez.net:8310/ordos/forward_price'
for i in range(1,5):
    url = native_url+'?refdate_from=' +date_from+ '&refdate_to=' +date_to+'&curve='+curve+'&mat=Y_0'+str(i)\
          +'&label=EC&rftype=PURE&mat=Y*'
    session = requests.Session()
    session.trust_env = False
    response = session.get(url)
    my_data = pd.read_json(response.content)
    final_data=final_data.append(my_data)
