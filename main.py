import datetime as dt
import sys, time, atexit
# Layout / Web import
from flask import Flask, request, render_template, flash, url_for, redirect, jsonify
from diskcache import Cache
import threading
# Computation imports
import pandas as pd
import random
#Bokeh imports
from bokeh.models import (HoverTool, FactorRange, LinearAxis, Grid,Range1d, Legend)
from bokeh.plotting import figure
from bokeh.embed import components
#Auto-reload
from flask_apscheduler import APScheduler
# Personnal files
import forms, DataHandling, CacheHandling, Dictionnaries, GraphHandling

# Checking is prod to change server from 5000 to 5001
IS_PROD = sys.argv[1] == "prod"
# Setting up cache timer
CACHE_TIMEOUT = 60*60*6
# Defining the Flask context
app = Flask(__name__, template_folder='Template')
# define the cache config :
cache = Cache(app.instance_path+'/cachedir/')
atexit.register(cache.clear)

@app.before_first_request
def load_database():
    _, _ = CacheHandling.get_my_cache("MRMCache",cache,CACHE_TIMEOUT)

@app.route("/")
@app.route("/tabs", methods=['GET','POST'])
def tab():
    return render_template("tabs.html")

@app.route("/chart", methods=['GET','POST'])
def chart():
    #Initialize for first display :
    div, divc, script, scriptc, listc= ('',)*5
    table = []
    testyear = []
    full_data_filter = pd.DataFrame()
    pd.set_option('display.float_format', lambda x: '%.1f' % x)
    data, datafull = CacheHandling.get_my_cache("MRMCache", cache, CACHE_TIMEOUT)

    if request.method == 'POST':

        # Get the selected year - if no year selected then display full

        fullyear = []
        mindelta = request.form.get('tablefilter')
        division = request.form.get('active')
        form['active'].data = division
        form['tablefilter'].data = mindelta
        for i in range(year_start,year_end):
            fullyear.append(str(i))
            if request.form.get('Check'+str(i)) is not None:
                form['Check'+str(i)].data = True
                testyear.append(str(i))
            else:
                form['Check' + str(i)].data = False
        if not testyear:
            testyear=fullyear

        # Filter the data from the dataframe returned by the query

        data_filter, full_data_filter = DataHandling.DataFilter(data, datafull, testyear, division, mindelta)
        data_country = DataHandling.DataFilterCountry(data, datafull, testyear, division, mindelta)

        # Table creation for display

        full_data_filter = full_data_filter.replace({'book' : Dictionnaries.AssetBooks})

        for year in testyear:

            table.append(full_data_filter.loc[full_data_filter.year == int(year)].to_html(index = False))

        # Bokeh hover creation
        hover = GraphHandling.create_hover_tool()
        plot = GraphHandling.create_chart(data_filter,'year', "MTM evolution of : " + division, "t0", "MtR_MtM1", hover )
        script, div = components(plot)

        country = ['BE','FR', 'NL','DE']
        listc = []
        scriptc = []
        divc = []
        for country in country:
            tempDF = data_country[data_country['strategy'] == country]
            tempDF = tempDF.drop('strategy',1)
            tempHover = GraphHandling.create_hover_tool()
            if not tempDF.empty:
                tempchart = GraphHandling.create_chart(tempDF, 'year',
                         "MTM evolution of : " + division, "t0", "MtR_MtM1",tempHover)
                tempscript, tempdiv = components(tempchart)
                scriptc.append(tempscript)
                listc.append(country)
                divc.append(tempdiv)

        # Last Minute Adjustment :
        testyear.insert(0, 'na')

    return render_template("chart.html", the_div=div, the_script=script,
                           form=form, tables=table, titles = testyear, scriptc = scriptc, divc = divc, listc=listc)

@app.route("/prices", methods=['GET','POST'])
def prices():
    power_prices=CacheHandling.get_my_cache("PriceCache"+"PW*",cache,CACHE_TIMEOUT)
    curve_list = ['PW_BE','PW_NL','PW_FR','PW_DE']
    power_plot = DataHandling.PricesFilter(power_prices,curve_list,'BS')
    hover = GraphHandling.create_hover_tool()
    PlotGroup = []
    ScriptGroup = []
    DivGroup =[]
    for index, curve in enumerate(curve_list):
        PlotGroup.append(GraphHandling.create_chart_prices(power_plot,'mat', curve , "Price evolution of "+ curve , "refdate","rfvalue"))
        tempscript, tempdiv = components(PlotGroup[index])
        ScriptGroup.append(tempscript)
        DivGroup.append(tempdiv)

    return render_template("prices.html", PlotGroup=PlotGroup, ScriptGroup=ScriptGroup, DivGroup=DivGroup)

@app.route("/asset", methods=['GET','POST'])
def asset():
    data, datafull = CacheHandling.get_my_cache("MRMCache", cache, CACHE_TIMEOUT)
    pd.set_option('display.float_format', lambda x: '%.1f' % x)
    booklist = datafull.book.drop_duplicates()
    assetform = forms.asset_form(booklist)
    if request.method == 'GET':
        return render_template("asset.html", form = assetform)
    if request.method == 'POST':
        book = request.form.get('book')
        division = request.form.get('bu')
        book_filter = DataHandling.BookFilter(data, datafull, book, division)
        hover = GraphHandling.create_hover_tool()
        plot = GraphHandling.create_chart(book_filter, 'year', "MTM evolution of : " + book, "t0", "MtR_MtM1",
                                          hover)
        script, div = components(plot)
        return render_template("asset.html", form=assetform, script = script, div=div)
    return redirect(url_for('asset'))

@app.route("/pick_book", methods=['GET'])
def pick_book():
    _ , datafull = CacheHandling.get_my_cache("MRMCache", cache, CACHE_TIMEOUT)
    bu = request.args.get('bu', '01', type=str)
    books = datafull[['book','division']].drop_duplicates()
    books = books[books['division']==bu]
    return books.to_json(orient="records")

class Config(object):
    JOBS = [
        {
            'id': 'get_my_MTM',
            'func': 'CacheHandling:get_my_cache',
            'args' : ['MRMCache', cache, CACHE_TIMEOUT],
            'trigger': 'interval',
            'seconds': 60*60,
        },
        {
            'id': 'get_my_Prices',
            'func': 'CacheHandling:get_my_cache',
            'args': ['PriceCachePW*', cache, CACHE_TIMEOUT],
            'trigger': 'interval',
            'seconds': 60 * 60,
        }

    ]

if __name__ == '__main__':
    app.config.from_object(Config())
    scheduler = APScheduler()
    scheduler.init_app(app)
    scheduler.start()
    global form, year_end, year_start
    year_start = dt.date.today().year
    year_end = year_start + 5
    if IS_PROD:
        form = forms.create_form(year_start, year_end)
        app.run(host='0.0.0.0',debug=False, port=5000, use_reloader=False)
    else:
        form = forms.create_form(year_start, year_end)
        app.run(debug=True, port=5001, use_reloader = False)


